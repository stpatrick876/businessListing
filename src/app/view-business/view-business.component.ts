import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BusinessActionEventType, IBusiness, IBusinessActionEvent, Mode } from '../common.interface';

@Component({
  selector: 'app-view-business',
  templateUrl: './view-business.component.html',
  styleUrls: ['./view-business.component.css']
})
export class ViewBusinessComponent implements OnInit {
  @Input() business: IBusiness;
  @Output() actionEvent: EventEmitter<IBusinessActionEvent> = new EventEmitter<IBusinessActionEvent>();

  constructor() { }

  ngOnInit() {
  }

  closeView() {
      this.actionEvent.emit({type: BusinessActionEventType.CANCEL, mode: Mode.VIEW});
  }
}
