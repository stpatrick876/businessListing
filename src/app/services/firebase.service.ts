import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';
import { IBusiness, ICategory } from '../common.interface';

@Injectable()
export class FirebaseService {

  private businessCollection: AngularFirestoreCollection<IBusiness>;
  private categoriesCollection: AngularFirestoreCollection<ICategory>;

  constructor(private afs: AngularFirestore) {
    this.businessCollection = afs.collection<IBusiness>('company');
    this.categoriesCollection = afs.collection<ICategory>('categories');


  }


  getBusinesses() {
    return this.businessCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as IBusiness;
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });
  }
  addBusiness(business: IBusiness) {
    return this.businessCollection.add(business);
  }
  getCategories() {
    return this.categoriesCollection.valueChanges();
  }
  deleteBusiness(business: IBusiness) {
   return this.businessCollection.doc(`/${business.id}`).delete();
  }

  updateBusiness(business: IBusiness) {
    console.log(business)
    return this.businessCollection.doc(`/${business.id}`).update(business);

  }

  filterBusinessByCategory(category: string) {
    return this.afs.collection('company', ref => ref.where('category', '==', category))
      .snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as IBusiness;
        const id = a.payload.doc.id;
        return { id, ...data };
      });
    });

  }



}
