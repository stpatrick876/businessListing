import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './services/firebase.service';
import { Observable } from 'rxjs/Observable';
import { BusinessActionEventType, IBusiness, IBusinessActionEvent, ICategory, Mode, Notification } from './common.interface';
import { DocumentChangeAction } from 'angularfire2/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
   businesses: IBusiness[];
   categories: Observable<ICategory[]>;
   openPane = false;
   notification: Notification;
   mode: Mode;
   modes = Mode;
   selectedBusiness: IBusiness;
   showAbout = false;
  loading = false;
  constructor(private _api: FirebaseService){
  }


  ngOnInit() {
    this.loading = true;
    this._api.getBusinesses().subscribe(businesses => {
      this.businesses = businesses;
      this.loading = false;

    });
    this.categories = this._api.getCategories();
  }

  openTopPane(mode: Mode, business?: IBusiness) {
    if (this.openPane) {
      return false;
    }
    this.selectedBusiness = business;
    this.mode = mode;
    this.openPane = true;
  }



  onActionEvent(event: IBusinessActionEvent) {
    console.log('event ', event);

    switch(event.type) {
      case BusinessActionEventType.SUCCESS:
        this.openPane = false;
        this.notification = {
          shown: true,
          message: `${this.modes[event.mode]} Business Successful`,
          css_class: 'success'
        };
        this.closeNotification();
        break;
      case BusinessActionEventType.FAIL:
        this.openPane = false;
        this.notification = {
          shown: true,
          message: `${this.modes[event.mode]} Business Failed`,
          css_class: 'alert'
        };
        this.closeNotification();
        break;
      case BusinessActionEventType.CANCEL:
        this.openPane = false;
        if(event.mode !== Mode.VIEW) {
          this.notification = {
            shown: true,
            message: `${this.modes[event.mode]} Business Canceled`,
            css_class: 'warning'
          };
          this.closeNotification();
        }
        break;
      default:

    }
  }


  closeNotification(){
    setTimeout(() => {
      this.notification.shown = false;
    }, 5000);
  }


  filterBusiness(cat: any) {
    this.loading = true;

    if(cat === 'all') {
      this._api.getBusinesses().subscribe(businesses => {
        this.businesses = businesses;
        this.loading = false;

      });
    } else {
      this._api.filterBusinessByCategory(cat).subscribe(businesses => {
        this.businesses = businesses;
        this.loading = false;

      });
    }

  }

}



