import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { FirebaseService } from './services/firebase.service';
import { EditBusinessComponent } from './edit-business/edit-business.component';
import { ViewBusinessComponent } from './view-business/view-business.component';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { AddBusinessComponent } from './add-business/add-business.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AboutComponent } from './about/about.component';
import { CustomFormsModule } from 'ng2-validation';
import { PhonePipe } from './common.pipe';
import { TextMaskModule } from 'angular2-text-mask';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  declarations: [
    AppComponent,
    EditBusinessComponent,
    ViewBusinessComponent,
    ConfirmDeleteComponent,
    AddBusinessComponent,
    AboutComponent,
    PhonePipe,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    ReactiveFormsModule,
    FormsModule,
    TextMaskModule

  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
