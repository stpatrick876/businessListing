export enum Category {
  RETAIL = 'Retail',
  INFO_TECK = 'IT',
  AUTO = 'Auto',
  CONTRUCTION = 'Construction'
}

export enum BusinessActionEventType{
  SUCCESS,
  FAIL,
  CANCEL
}

export enum Mode {
  EDIT,
  VIEW,
  DELETE,
  ADD
}

export interface ICategory {
  $key?: string;
  name: Category;
}


export interface IBusiness {
  id?: string;
  category: ICategory;
  name?: string;
  description?: string;
  email?: string;
  street_address?: string;
  state?: string;
  city?: string;
  zipcode?: string;
  phone?: string;
  years_in_business: number;
}

export interface IBusinessActionEvent {
  type: BusinessActionEventType;
  mode: Mode;
  payload?: any;
}

export interface Notification {
  shown: boolean;
  message: string;
  css_class: string;
}

