import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BusinessActionEventType, IBusiness, IBusinessActionEvent, ICategory, Mode } from '../common.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { FirebaseService } from '../services/firebase.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-edit-business',
  templateUrl: './edit-business.component.html',
  styleUrls: ['./edit-business.component.css']
})
export class EditBusinessComponent implements OnInit {
  @Input() business: IBusiness;
  @Output() actionEvent: EventEmitter<IBusinessActionEvent> = new EventEmitter<IBusinessActionEvent>();
  editBusinessForm: FormGroup;
  categories: Observable<ICategory[]>;
  cat: any;

  constructor(private _api: FirebaseService, private fb: FormBuilder) { }

  ngOnInit() {
    this.categories = this._api.getCategories();
    this.editBusinessForm = this.fb.group({
      name: [''],
      description: [''],
      email: [''],
      street_address: [''],
      state: [''],
      city: [''],
      zipcode: [''],
      phone: [''],
      years_in_business: ['']
    });

    if(!isNullOrUndefined(this.business)) {
      this.editBusinessForm.controls['name'].setValue(this.business.name);
      this.editBusinessForm.controls['description'].setValue(this.business.description);

      this.editBusinessForm.controls['email'].setValue(this.business.email);
      this.editBusinessForm.controls['street_address'].setValue(this.business.street_address);
      this.editBusinessForm.controls['state'].setValue(this.business.state);
      this.editBusinessForm.controls['city'].setValue(this.business.city);
      this.editBusinessForm.controls['zipcode'].setValue(this.business.zipcode);
      this.editBusinessForm.controls['phone'].setValue(this.business.phone);
      this.editBusinessForm.controls['years_in_business'].setValue(this.business.years_in_business);
      this.cat = this.business.category;
    }
  }

  editBusiness(formVal: any, cat: ICategory) {
    formVal['category'] = cat['value'];
    formVal['id'] = this.business.id;

    this._api.updateBusiness(formVal).then((res) => {
      console.log('res ', res)
      this.actionEvent.emit({type: BusinessActionEventType.SUCCESS, mode: Mode.EDIT, payload: res});
    }).catch(err => {
      console.log('err ', err)

      this.actionEvent.emit({type: BusinessActionEventType.FAIL, mode: Mode.EDIT});
    });

  }

  cancelAdd() {
    this.actionEvent.emit({type: BusinessActionEventType.CANCEL, mode: Mode.EDIT});
  }


}
