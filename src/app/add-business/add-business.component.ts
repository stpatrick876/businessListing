import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from '../services/firebase.service';
import { BusinessActionEventType, IBusinessActionEvent, ICategory, Mode } from '../common.interface';
import { Observable } from 'rxjs/Observable';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-add-business',
  templateUrl: './add-business.component.html',
  styleUrls: ['./add-business.component.css']
})
export class AddBusinessComponent implements OnInit {
  addBusinessForm: FormGroup;
  categories: Observable<ICategory[]>;
  category: ICategory;
  @Output() actionEvent: EventEmitter<IBusinessActionEvent> = new EventEmitter<IBusinessActionEvent>();
  phoneMask: any[] = ['+', '1', ' ', '(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(private fb: FormBuilder, private _api: FirebaseService) { }

  ngOnInit() {
    this.categories = this._api.getCategories();
    this.addBusinessForm = this.fb.group({
      name: [''],
      description: [''],
      email: ['', CustomValidators.email],
      street_address: [''],
      state: [''],
      city: [''],
      zipcode: [''],
      phone: [''],
      years_in_business: ['']
    });
  }

  addBusiness(formVal: any, cat: ICategory) {
    formVal['category'] = cat['value'];
    this._api.addBusiness(formVal).then((res) => {
      console.log('res ', res)
      this.actionEvent.emit({type: BusinessActionEventType.SUCCESS, mode: Mode.ADD, payload: res});
    }).catch(err => {
      this.actionEvent.emit({type: BusinessActionEventType.FAIL, mode: Mode.ADD});
    });

  }

  cancelAdd() {
    this.actionEvent.emit({type: BusinessActionEventType.CANCEL, mode: Mode.ADD});
  }

}
