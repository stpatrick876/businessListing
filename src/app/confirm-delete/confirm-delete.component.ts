import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BusinessActionEventType, IBusiness, IBusinessActionEvent, Mode } from '../common.interface';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.css']
})
export class ConfirmDeleteComponent implements OnInit {
  @Input() business: IBusiness;
  @Output() actionEvent: EventEmitter<IBusinessActionEvent> = new EventEmitter<IBusinessActionEvent>();

  constructor(private _api: FirebaseService) { }

  ngOnInit() {
    console.log(this.business, 'business is')
  }

  deleteBusiness() {
    this._api.deleteBusiness(this.business).then((res) => {
      console.log('res ', res)
      this.actionEvent.emit({type: BusinessActionEventType.SUCCESS, mode: Mode.DELETE});
    }).catch(err => {
      this.actionEvent.emit({type: BusinessActionEventType.FAIL, mode: Mode.DELETE});
    });
  }

  cancelDelete() {
    this.actionEvent.emit({type: BusinessActionEventType.CANCEL, mode: Mode.DELETE});
  }

}
