// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCoRSR2qyb5Mk2EDtwUjLLoCBxuJoi_pXk',
    authDomain: 'business-listing-63a6f.firebaseapp.com',
    databaseURL: 'https://business-listing-63a6f.firebaseio.com',
    projectId: 'business-listing-63a6f',
    storageBucket: 'business-listing-63a6f.appspot.com',
    messagingSenderId: '393683929301'
  }
};
